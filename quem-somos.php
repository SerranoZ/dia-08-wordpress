<?php
// Template Name: Quem somos
?>
    <?php get_header(); ?>

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/quem-somos.css">
    

    <main>
        <h1><?php the_title(); ?></h1>
        <p><?php the_content(); ?>
        </p>
    </main>
    <?php get_footer(); ?>

