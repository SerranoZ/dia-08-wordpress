<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home Page</title>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/style.css">
    <?php wp_head(); ?>
</head>
<body>
    <header>
        <a class="links-header" href="lista-lobinhos.html" target="_self">Nossos Lobinhos</a>
        <a href="home-page.html">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/Imagens/img-lobinho-header.png" alt="Lobinho-Feliz">
        </a>
        <a class="links-header" href="quem-somos.html" target="_self">Quem Somos</a>
    </header>