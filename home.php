<?php
// Template Name: Home page
?>
    <?php get_header(); ?>

    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/home.css">
    <main>
        <section class = "adote">
            <div class="in-adote">
                <h1 class="titulo-adote">Adote um Lobinho</h1>
                <h2> É claro que o consenso sobre a necessidade de qualificação apresenta tendências no sentido de aprovar a manutenção das regras de conduta normativas.</h2>
            </div>
        </section>
        <section class = "sobre">
            <h1 class="titulos">Sobre</h1>
            <p class="paragrafo">Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas. Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.Não obstante, o surgimento do comércio virtual faz parte de um processo de gerenciamento do levantamento das variáveis envolvidas.</p>
        </section>
        <section class = "valores">
            <h1 class="titulos">Valores</h1>
            <div class="valores2">
                
                <div class="valores3">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/Imagens/protecao.png" alt="">
                    <h2>Proteção</h2>
                    <p class="conteudo">
                        Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
                    </p>
                </div>
                
                <div class="valores3">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/Imagens/carinho.png" alt="">
                    <h2>Carinho</h2>
                    <p class="conteudo">
                        Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
                    </p>
                </div>
                
                <div class="valores3">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/Imagens/companheirismo.png" alt="">
                    <h2>Companheirismo</h2>
                    <p class="conteudo">
                        Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
                    </p>
                </div>
                
                <div class="valores3">
                    <img src="<?php echo get_stylesheet_directory_uri() ?>/Imagens/resgate.png" alt="">
                    <h2>Resgate</h2>
                    <p class="conteudo">
                        Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral.
                    </p>
                </div>
            
            </div>
        </section>

        <ul class = "lista-lobinhos">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <li>
                <?php if( get_field('link_imagem') ): ?>
                    <img src="<?php the_field('link_imagem'); ?>" />
                <?php endif; ?>

                <div class="bloco0">
                    <div class="bloco1">
                        <div>
                            <h1>Nome: <?php the_field('nome_lobinho'); ?></h1>
                            <h2>Idade: <?php the_field('idade'); ?> anos</h2>
                        </div>
                        <a href="adotar-lobinho.html?lobinho=${wolf.id}">
                            <input class="adotar" type="button" value="Adotar" target="_self">
                        </a>
                    </div>
                    <p class="texto"><?php the_field('descricao'); ?></p>
                </div> 
            </li>
            <?php endwhile; else: ?>
                <p>desculpe, o post não segue os critérios escolhidos</p>
            <?php endif; ?>
        </ul>
    </main>

    <?php get_footer(); ?>
    

    
