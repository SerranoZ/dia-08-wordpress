<?php
// Template Name: Lista Lobinhos
?>

    <?php get_header(); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/lista-lobinhos.css">

    <main>
        <div class="cima">
            <div id="pesquisa-add" class="pesquisa-add">
                <img class="patinhafofa" src="Imagens/patinhafofa.png" alt="Lobinho-Feliz">
                <input id="pesquisa" class="pesquisa" type="text" onkeyup="myFunction()">
                <a href="adicionar-lobinho.html"><button>+ LOBO</button></a>
            </div>
            <div class="lobinhos-adotados">
                <input class="checkbox" id="checkbox" type="checkbox" name="" value="on" onclick="if(this.checked){getAdoptedLobinhos()}">
                <p>Ver lobinhos adotados</p>
            </div>
        </div>
        <ul class = "lista-lobinhos">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <li>
                <?php if( get_field('link_imagem') ): ?>
                    <img src="<?php the_field('link_imagem'); ?>" />
                <?php endif; ?>

                <div class="bloco0">
                    <div class="bloco1">
                        <div>
                            <h1>Nome: <?php the_field('nome_lobinho'); ?></h1>
                            <h2>Idade: <?php the_field('idade'); ?> anos</h2>
                        </div>
                        <a href="adotar-lobinho.html?lobinho=${wolf.id}">
                            <input class="adotar" type="button" value="Adotar" target="_self">
                        </a>
                    </div>
                    <p class="texto"><?php the_field('descricao'); ?></p>
                </div> 
            </li>
            <?php endwhile; else: ?>
                <p>desculpe, o post não segue os critérios escolhidos</p>
            <?php endif; ?>     
        </ul>
    </main>

    <?php get_footer(); ?>