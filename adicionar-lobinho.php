<?php
// Template Name: adicionar lobinho
?>  
    <?php get_header(); ?>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri() ?>/adicionar-lobinho.css">
    <section>
        <div>
            <h1 style="text-align: center;">
                Coloque um Lobinho para adoção
            </h1>
        </div>  
        <div class="container">
            
            <div class="subcontainer">
                <div>
                    <h2 class="nome">Nome do Lobinho:</h2>
                    <input  class = "name" type="text">
                </div>
                <div class="idade">
                    <h2 class="nome">Anos:</h2>
                    <input class = "age" type="text">
                </div>
            </div>
        
            <div class="foto">
                <h2 class="nome">Link da foto:</h2>
                <input class = "image-url" type="text">
            </div>
          
            <div class="descricao">
                <h2 class="nome">Descrição:</h2>
                <input class="description" type="text">
            </div>
        </div>

        <input class = "botao-salvar" type="button" value="Salvar">
    </section>

    <?php get_footer(); ?>

    

