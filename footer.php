<footer>
        <div class="map-items">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1837.591575960131!2d-43.13395577682204!3d-22.906614469764524!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x99817e444e692b%3A0xfd5e35fb577af2f5!2sUFF%20-%20Instituto%20de%20Computa%C3%A7%C3%A3o!5e0!3m2!1spt-BR!2sbr!4v1645301952747!5m2!1spt-BR!2sbr" width="300" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

            <ul class="lista">
                <li><img class="icones" src="<?php echo get_stylesheet_directory_uri() ?>/Imagens/mapa.png" alt=""> Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem, Niterói - RJ, 24210-315</li>
                <li><img class="icones" src="<?php echo get_stylesheet_directory_uri() ?>/Imagens/telefone.png" alt="">(99) 99999-9999</li>
                <li><img class="icones" src="<?php echo get_stylesheet_directory_uri() ?>/Imagens/email.png" alt="">salve-lobos@lobINhos.com</li>
                <li><a href="quem-somos.html" target="_self"><input class="butao" type="button" value="Quem Somos"></a></li>
            </ul>
        </div>
        <div class="patinha">
            <p style="margin: 0px 0px 5px 0px; color: white;">Desenvolvido com</p>
            <img class="tamanho-pata" src="<?php echo get_stylesheet_directory_uri() ?>/Imagens/paws.png" alt="patinha">
        </div>
    </footer>

    <script src="home-page.js"></script>
    <?php wp_footer(); ?>
</body>
</html>